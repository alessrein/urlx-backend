<?php
use App\Link;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/l/{code}', function ($code) {
    $link = Link::where('code', '=', $code)->first();
    if ($link)
    {
        $link->visits += 1;

        $link->save();
        
        return redirect($link->url);
        
    } else abort(404);
});




