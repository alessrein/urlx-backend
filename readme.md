# URLX (Backend)
An amazingly simple URL Shortener

---------------------------------------

## Database
This project uses SQLite as its database engine. The file should be located at `./database/database.sqlite/`

## Logic
The code that handles the logic is distributed inside:

* `routes/web.php`
* `routes/api.php`
* `app/Link.php`

## Algorithm
The algorithm used here is pretty simple. First, we save the link in the database, then we take the autoincrementing ID of the link record (link.id), and convert it to base64. This will generate a string of 6 characters aprox. URLs previously saved, will just be returned from the database.

## Setup
First, https://getcomposer.org 

1. Clone this repository and then follow the steps below.
2. Run `composer install` to install dependencies
3. Create a `.env` file from `.env.example`.
4. Create a file called `database.sqlite` inside `database/` directory.
5. Update `DB_DATABASE=/absolute/path/to/database.sqlite` variable inside the `.env` file. You must use an absolute path.
6. Run `php artisan serve`.

Now the server is ready for requests.

## Testing
To run tests use the following command:

`./vendor/bin/phpunit`