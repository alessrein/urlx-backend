<?php

namespace Tests\Unit;

use Tests\TestCase;
// use Illuminate\Foundation\Testing\WithFaker;
// use Illuminate\Foundation\Testing\RefreshDatabase;

class LinkTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLinkEncode ()
    {
        
        $this->assertEquals("AQAAAA", \App\Link::encode(1), "it should encode a number to a 6 digit base64 string");
    }

    public function testLinkTitle () {
        $this->assertEquals("Google", \App\Link::getTitle("http://google.com", "it should return the correct title of a page"));
    }
}
