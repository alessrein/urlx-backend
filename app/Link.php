<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{

    public function shorten() {
        $this->code =  self::encode($this->id);
        $this->save();
    }

    /**
     * Encode the ID of the record as base64, this will be
     * the code assigned to the URL.
     */
    public static function encode($id) {
        return strtr(rtrim(base64_encode(pack('i', $id)), '='), '+/', '-_');
    }

    /**
     * Get the title of the URL
     */
    public static function getTitle($url) {
        $str = file_get_contents($url);
        if(strlen($str)>0){
            // $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
            preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title); // ignore case
            return $title[1];
        }
    }
}
