<?php

use App\Link;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/shrink', function(Request $request) {
    $url = $request->input('url');

    // validator with rules
    $validator = \Validator::make($request->all(), [
        'url' => 'url|required',
    ]);
    
    // if validator fails return object with array of errors
    if ($validator->fails()) 
        return response(['errors' => $validator->errors()->all() ], 422);

    // if a link with same url is found will return it
    $link = Link::where('url', '=', $url)->first();

    if (! $link) {
        $link = new Link;
        $link->url = $url;
        $link->title = Link::getTitle($url);
        if ($link->save()) {
            $link->shorten();
        }
    }

    return response($link, 200)->header('Content-Type', 'application/json');
});


Route::get('/top100', function () {
    return Link::orderBy('visits', 'desc')->limit(100)->get();
});

